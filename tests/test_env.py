import pytest
from lanternfish.env import Env


@pytest.fixture
def _before_each(monkeypatch):
    monkeypatch.setenv('PROJECT_ID', 'MyTestProjectID')
    monkeypatch.setenv('PRIVATE_TOKEN', 'MyTestPrivateToken')


def test_get_project_id(_before_each):
    assert Env.get_project_id() == 'MyTestProjectID'


def test_get_private_token(_before_each):
    assert Env.get_private_token() == 'MyTestPrivateToken'


def test_get_gitlab_base_url(_before_each):
    assert Env.get_gitlab_base_url() == 'https://gitlab.com/api/v4/projects/'


def test_get_badges_url(_before_each):
    assert Env.get_badges_url() == 'http://northwoods-badges.s3-website-us-east-1.amazonaws.com/traverse/deploy/deploy_status.html'
