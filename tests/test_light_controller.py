# pylint: disable=no-member
from Mock import GPIO
from lanternfish.light_controller import LightController


def test_init(mocker):
    mocker.patch.object(GPIO, 'setwarnings')
    mocker.patch.object(GPIO, 'setmode')
    mocker.patch.object(GPIO, 'setup')
    mocker.patch.object(GPIO, 'cleanup')

    l_c = LightController()
    l_c.init()

    GPIO.setwarnings.assert_called_once_with(False)
    GPIO.setmode.assert_called_once_with(GPIO.BCM)

    GPIO.setup.assert_has_calls(
        [
            mocker.call(4, GPIO.OUT),
            mocker.call(22, GPIO.OUT),
            mocker.call(6, GPIO.OUT),
            mocker.call(26, GPIO.OUT),
        ]
    )
    assert GPIO.setup.call_count == 4

    GPIO.cleanup.assert_called_once()


def test_red_light(mocker):
    mocker.patch.object(GPIO, 'output')

    l_c = LightController()
    l_c.turn_on_red_light()

    calls = [
        mocker.call(4, GPIO.HIGH),
        mocker.call(22, GPIO.LOW),
        mocker.call(6, GPIO.LOW),
        mocker.call(26, GPIO.LOW),
    ]

    GPIO.output.assert_has_calls(calls, any_order=True)
    assert GPIO.output.call_count == 4


def test_yellow_light(mocker):
    mocker.patch.object(GPIO, 'output')

    l_c = LightController()
    l_c.turn_on_yellow_light()

    calls = [
        mocker.call(4, GPIO.LOW),
        mocker.call(22, GPIO.HIGH),
        mocker.call(6, GPIO.LOW),
        mocker.call(26, GPIO.LOW),
    ]

    GPIO.output.assert_has_calls(calls, any_order=True)
    assert GPIO.output.call_count == 4


def test_green_light(mocker):
    mocker.patch.object(GPIO, 'output')

    l_c = LightController()
    l_c.turn_on_green_light()

    calls = [
        mocker.call(4, GPIO.LOW),
        mocker.call(22, GPIO.LOW),
        mocker.call(6, GPIO.HIGH),
        mocker.call(26, GPIO.LOW),
    ]

    GPIO.output.assert_has_calls(calls, any_order=True)
    assert GPIO.output.call_count == 4


def test_buzzer(mocker):
    mocker.patch.object(GPIO, 'output')

    l_c = LightController()
    l_c.turn_on_buzzer()

    calls = [
        mocker.call(4, GPIO.LOW),
        mocker.call(22, GPIO.LOW),
        mocker.call(6, GPIO.LOW),
        mocker.call(26, GPIO.LOW),
        mocker.call(26, GPIO.HIGH),
        mocker.call(26, GPIO.LOW),
    ]

    GPIO.output.assert_has_calls(calls, any_order=True)
    assert GPIO.output.call_count == 6
