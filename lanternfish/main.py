import requests as r
import models.pipeline as pip
from env import Env
from light_controller import LightController


def get_pipeline_id():
    url = Env.get_badges_url()
    headers = {'user-agent': 'lanternfish/0.0.1'}

    resp = r.get(url, headers=headers, timeout=30)

    return resp.text.strip('#<meta http-equiv="refresh" content="0; URL=\'https://gitlab.com/northwoods/traverse/pipelines/').strip("'\" />\n")


def get_pipeline_detail(pipeline_id):
    url = f'https://gitlab.com/api/v4/projects/14353791/pipelines/{pipeline_id}'
    headers = {
        'user-agent': 'lanternfish/0.0.1',
        'PRIVATE-TOKEN': Env.get_private_token(),
    }

    resp = r.get(url, headers=headers, timeout=30)

    pipeline = pip.Pipeline.from_obj(resp.json())

    lc = LightController()

    match pipeline.status:
        case 'running':
            print('Yellow')
            lc.turn_on_yellow_light()
        case 'success':
            print('Green')
            lc.turn_on_green_light()
        case 'failed':
            print('Red')
            lc.turn_on_red_light()
        case _:
            print('Unsupported pipeline status')


if __name__ == '__main__':
    p_id = get_pipeline_id()
    get_pipeline_detail(p_id)
