import os
import dotenv

DOTENV_PATH = '.env'
dotenv.load_dotenv()


class Env:
    @staticmethod
    def get_project_id():
        return os.getenv('PROJECT_ID')

    @staticmethod
    def get_private_token():
        return os.getenv('PRIVATE_TOKEN')

    @staticmethod
    def get_gitlab_base_url():
        return 'https://gitlab.com/api/v4/projects/'

    @staticmethod
    def get_badges_url():
        return 'http://northwoods-badges.s3-website-us-east-1.amazonaws.com/traverse/deploy/deploy_status.html'
