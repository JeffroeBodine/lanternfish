import json


class Pipeline:
    def __init__(
        self,
        id,
        ref,
        status,
        created_at,
        updated_at,
        started_at,
        finished_at,
    ):
        self.id = id
        self.ref = ref
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.started_at = started_at
        self.finished_at = finished_at

    def __eq__(self, other):
        if not isinstance(other, Pipeline):
            return NotImplemented

        return (
            self.id == other.id
            and self.ref == other.ref
            and self.status == other.status
            and self.created_at == other.created_at
            and self.updated_at == other.updated_at
            and self.started_at == other.started_at
            and self.finished_at == other.finished_at
        )

    def to_json(self):
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)

    @classmethod
    def from_obj(cls, obj):
        return Pipeline(
            obj['id'],
            obj['ref'],
            obj['status'],
            obj['created_at'],
            obj['updated_at'],
            obj['started_at'],
            obj['finished_at'],
        )
