from time import sleep

try:
    import RPi.GPIO as GPIO
except ImportError:
    import Mock.GPIO as GPIO

R = 4
Y = 22
G = 6
B = 26


class LightController:
    @staticmethod
    def init():
        print('Initializing GPIO')

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(R, GPIO.OUT)
        GPIO.setup(Y, GPIO.OUT)
        GPIO.setup(G, GPIO.OUT)
        GPIO.setup(B, GPIO.OUT)

        GPIO.cleanup()

    def turn_on_red_light(self):
        self.turn_off_all_but(R)
        GPIO.output(R, GPIO.HIGH)

    def turn_on_yellow_light(self):
        self.turn_off_all_but(Y)
        GPIO.output(Y, GPIO.HIGH)

    def turn_on_green_light(self):
        self.turn_off_all_but(G)
        GPIO.output(G, GPIO.HIGH)

    def turn_on_buzzer(self):
        self.turn_off_all_but(-1)
        GPIO.output(B, GPIO.HIGH)
        sleep(0.25)
        GPIO.output(B, GPIO.LOW)

    def turn_off_all_but(self, light):
        if light != R:
            GPIO.output(R, GPIO.LOW)
        if light != Y:
            GPIO.output(Y, GPIO.LOW)
        if light != G:
            GPIO.output(G, GPIO.LOW)

        GPIO.output(B, GPIO.LOW)


if __name__ == '__main__':
    LightController.init()
