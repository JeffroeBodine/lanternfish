from setuptools import setup, find_packages
import os

VERSION = os.environ.get('PACKAGE_VERSION', '0.0.0')
DESCRIPTION = 'Traffic Light Controller'
LONG_DESCRIPTION = 'Traffic light controller that changes state based on CI deployment jobs'

setup(
    name='LanternFish',
    version=VERSION,
    author='Jeffroe',
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    license='MIT',
    packages=find_packages(),
    keywords=['python', 'traffic light, ci/cd, build radiator'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Personal',
        'Programming Language :: Python :: 3',
    ],
)
